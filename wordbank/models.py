from django.db import models

class Word(models.Model):
    word_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.word_text
    
class Detail(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    type_text= models.CharField(max_length=200)
    mean_text= models.CharField(max_length=200)
    sentence_text= models.CharField(max_length=200)
    
