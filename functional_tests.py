from selenium import webdriver
import unittest

class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
	def tearDown(self):
		self.browser.quit()
	def test_can_start_a_list_and_retrieve_it_later(self):
		# Edith has heard about a cool new online wordbank app. She goes
		# to check out its homepage
		self.browser.get('http://localhost:8000')
		# She notices the page title and header mention word bank
		self.assertIn('wordbank', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('Word Bank', header_text)

		
if __name__ == '__main__': 
	unittest.main(warnings='ignore')
