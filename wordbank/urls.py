from django.conf.urls import url

from . import views

app_name = 'wordbank'
urlpatterns = [
    url(r'^$', views.home_page, name='home_page'),
#-------------------------search----------------------------------------#
    url(r'^latest/$', views.latest_word.as_view(), name='latest_word'),
#-------------------------add word----------------------------------------#
    url(r'^into_add_page/$', views.into_add_page, name='add_new_word'),
    url(r'^save_new_word/$', views.save_new_word, name='check_error'),
    url(r'^already_word/$', views.save_new_word, name='already_word'),
    url(r'^slips/$', views.save_new_word, name='check_error'),
    
#-------------------------search----------------------------------------#
    url(r'^all/$', views.AllView.as_view(), name='all_word'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='details'),
#-------------------------search----------------------------------------#
    url(r'^search/$', views.word_search, name='search_into_word'),
]
