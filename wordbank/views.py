
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone

import csv
import os

from .models import Word, Detail


def home_page(request):
    return render(request,'wordbank/home_page.html')
   
class latest_word(generic.ListView):
    template_name = 'wordbank/latest_word.html'
    context_object_name = 'latest_word_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Word.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Word
    template_name = 'wordbank/details.html'
    def get_queryset(self):
       return Word.objects.filter(pub_date__lte=timezone.now())

class AllView(generic.ListView):
    template_name = 'wordbank/all_word.html'
    context_object_name = 'all_word_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Word.objects.order_by('word_text').all
#----------------- add word -------------------------#


def word_search(request):
    if request.method == 'POST':
        vocab = str(request.POST['search_into_word'])
        if Word.objects.filter(word_text=vocab):
            word = Word.objects.get(word_text=vocab)
            return render(request,'wordbank/search_into_word.html',{'word':word})
        else:
            not_found = ("not found word  : " +'"'+ str(vocab)+'"')
            return render(request,'wordbank/home_page_search.html',{'not_found':not_found})
    return HttpResponseRedirect(reverse('wordbank:home_page'))
           

#----------------------------------------------------------------------
def into_add_page(request):
    return render(request,'wordbank/add_new_word.html')

def save_new_word(request):
    slips = None
    
    if request.method == 'POST':
        add_word = str(request.POST['add_word'])
        add_type = request.POST['add_type']
        add_mean = request.POST['add_mean']
        add_sentence = request.POST['add_sentence']
        add_datetime = timezone.now()

        
        if add_word == "" or add_type == "" or add_mean == "" or add_sentence == "" :#check add data 
            slips = 'please add all field '      
            return render(request,'wordbank/check_error.html',{'slips':slips})

        elif Word.objects.filter(word_text=add_word):
            already_word = Word.objects.get(word_text=add_word)
            already_word.detail_set.create(type_text = add_type,
                                        mean_text =add_mean,
                                        sentence_text=add_sentence  
                                        )
        else:
            new_word = Word(
            word_text = add_word,
            pub_date  = add_datetime
            )
            new_word.save()
            new_word.detail_set.create(type_text = add_type,
                                       mean_text =add_mean,
                                       sentence_text=add_sentence  
                                       )
            
    return HttpResponseRedirect(reverse('wordbank:home_page'))

#-------------------------backup----------------------------------------#
def backup(request):
    list_word = Word.objects.order_by('word_text')
    with open('wordbank/backup/backup.csv', 'w') as csvfile:
        fieldnames = ['word', 'date', 'type', 'mean',  'example']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for word in list_word:
            for detail in word.detail_set.all():
                writer.writerow({'word':word.word_text, 'date':word.pub_date, 'type':      detail.type_text, 'mean': detail.mean_text,
                 'example': detail.sentence_text})


